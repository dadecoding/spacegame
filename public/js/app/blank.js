/*
*
*	----
*
*/

define(['Phaser'], function(Phaser) {

	function Space(game) {
		this.game = game;
	}

	Space.prototype = {
		constructor: Space,

		create: function() {

		},

		update: function() {

		}

	}

	return Space;

});