requirejs.config({
	baseUrl: "js/",
	paths: {
		Phaser: 'lib/phaser.min',
		SpaceGame: 'app/modules/SpaceGame',
		
		Boot: 'app/modules/states/Boot',
		Preload: 'app/modules/states/Preload',
		Space: 'app/modules/states/Space',

		Ship: 'app/modules/units/Ship',
		Asteroids: 'app/modules/units/Asteroids',

		Shooting: 'app/modules/factories/Shooting'
	}
});

require([
	'SpaceGame',
	'Boot',
	'Preload',
	'Space'
], function(SpaceGame, Boot, Preload, Space) {

	var game = new SpaceGame(500, 500);
		game.state.add('Boot', Boot);
		game.state.add('Preload', Preload);
		game.state.add('Space', Space);

		game.state.start('Boot');

});