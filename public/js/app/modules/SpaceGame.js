define(['Phaser'], function(Phaser) {


	function SpaceGame(w, h) {
		return new Phaser.Game(w, h, Phaser.AUTO, 'SpaceGame');
	}

	return SpaceGame;

});