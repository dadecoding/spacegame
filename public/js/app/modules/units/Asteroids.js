/*
*
*	Asteroids Functionality
*
*/

define(['Phaser'], function(Phaser) {

	
	function Asteroids(game, level) {
		this.game = game;
		this.level;

		if (typeof level == 'undefined') {
			this.level = this.game.rnd.integerInRange(1, 3);
		} else {
			this.level = level;
		}
		
		this.init();
	}

	Asteroids.prototype = Object.create(Phaser.Sprite.prototype);
	Asteroids.prototype.constructor = Asteroids;

	Asteroids.prototype.init = function() {
		var aX = this.game.rnd.integerInRange(200, 400);
		var aY = this.game.rnd.integerInRange(200, 400);
		Phaser.Sprite.call(this, this.game, aX, aY, 'asteroid'+this.level);
		this.game.physics.p2.enable(this);
		this.name = 'Asteroid';
		this.hp = 3;
		this.body.clearShapes();
		this.body.loadPolygon('physicsData', 'asteroid'+this.level);

		this.body.onEndContact.add(this.asteroidCollide, this);
		this.game.add.existing(this);
	}

	Asteroids.prototype.asteroidCollide = function() {
		// TODO
	};

	Asteroids.prototype.gotHit = function(bullet, eq) {
		console.log(eq);
		console.log(bullet.body.x, bullet.body.y);
		console.log(bullet.x, bullet.y);
		var explosion;
			explosion = this.game.add.sprite(0, 0, 'explosion');
			explosion.animations.add('explode', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0], 5, false);
			explosion.x = bullet.body.x;
			explosion.y = bullet.body.y;
			// explosion.x = eq.a;
			// explosion.y = eq.b;

		bullet.kill();

		this.hp--;
		this.frame++;

		if (this.hp == 0 && this.level > 1) {
			this.kill();
			this.devide(this.level - 1);
		} else if (this.hp == 0 && this.level == 1) {
			this.kill();
		}

		explosion.animations.play('explode', 15, false);
	}

	Asteroids.prototype.devide = function(level) {
		for (var i=0; i != 2; i++) {
			new Asteroids(this.game, level);
		}
	};

	return Asteroids;
});