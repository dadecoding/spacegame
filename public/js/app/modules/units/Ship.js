/*
*
*	Ship behaviour
*
*/

define(['Phaser', 'Shooting'], function(Phaser, Shooting) {

	function Ship(game, x, y) {
		Phaser.Sprite.call(this, game, x, y, 'ship');

		this.game.physics.p2.enable(this);
		this.name = 'Ship';
		this.body.clearShapes();
		this.body.loadPolygon('physicsData', 'ship');

		this.body.onBeginContact.add(this.shipCollide, this);

		this.shooting = new Shooting(this);


	}

	Ship.prototype = Object.create(Phaser.Sprite.prototype);
	Ship.prototype.constructor = Ship;

	Ship.prototype.shipCollide = function(target) {
		if (target) {
			if (target.sprite.name == 'Asteroid') {
				var explosion;
					explosion = this.game.add.sprite(0, 0, 'explosion');
					explosion.animations.add('explode', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0], 5, false);
					explosion.x = this.x;
					explosion.y = this.y;

				this.kill();
				explosion.animations.play('explode', 15, false);
			}
		}
	};

	Ship.prototype.fire = function() {
		this.shooting.shoot();
	}

	return Ship;

});