/*
*
*	Shooting behaviour
*
*/

define(['Phaser'], function(Phaser) {

	var bullets, activeBullet, explosion, bulletTime = 0;
	function Shooting(ship) {
		this.game = ship.game;
		this.ship = ship;

		bullets = this.game.add.group();
		bullets.createMultiple(20, 'bullet');

		explosion = this.game.add.sprite(0, 0, 'explosion');
		explosion.animations.add('explode', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0], 5, false);

	}

	Shooting.prototype = Object.create(Phaser.Sprite.prototype);
	Shooting.prototype.constructor = Shooting;

	Shooting.prototype.shoot = function() {
		if (this.game.time.now > bulletTime) {
			bulletTime = this.game.time.now + 500;
			var bullet = bullets.getFirstExists(false);
			if (bullet) {
				bullet.exists = true;
				bullet.name = 'Bullet';
				bullet.lifespan = 2500;
				this.game.physics.p2.enable(bullet);
				bullet.body.setCircle(6);

				bullet.reset(this.ship.x + 10, this.ship.y + 10);
				var bulletAngle = this.ship.body.angle;

				var bulletProperAngle = (this.ship.body.angle - 90) * Math.PI / 180;

				bullet.body.x = this.ship.x + (Math.cos(bulletProperAngle) * 26);
				bullet.body.y = this.ship.y + (Math.sin(bulletProperAngle) * 26);

				bullet.body.velocity.x = Math.cos(bulletProperAngle) * 400;
				bullet.body.velocity.y = Math.sin(bulletProperAngle) * 400;

				activeBullet = bullet;
				bullet.body.onBeginContact.add(this.bulletHit);
			}
		}
	};

	Shooting.prototype.bulletHit = function(target, shapeA, shapeB, eq) {
		//console.log(shapeA, shapeB, eq)
		if (target) {
			if (target.sprite.name == 'Asteroid') {
				target.sprite.gotHit(activeBullet, eq);
			}
		}
	}

	return Shooting;

});