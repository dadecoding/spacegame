/*
*
*	Game World initialization
*
*/

define(['Phaser', 'Ship', 'Asteroids'], function(Phaser, Ship, Asteroids) {

	function Space(game) {
		this.game = game;
		this.player;
		this.asteroids = [];
	}

	Space.prototype = {
		constructor: Space,

		create: function() {
			this.game.physics.startSystem(Phaser.Physics.P2JS);
			this.game.stage.backgroundColor = "#000";
			this.game.add.tileSprite(0, 0, 500, 500, 'bg');

			// Add player to space
			this.player = new Ship(this.game, 50, 50);
			this.game.add.existing(this.player);

			// Add asteroid to space
			for (var i=0; i != 1; i++) {
				this.asteroid = new Asteroids(this.game);
			}
			
			// Controls configuration
			this.game.cursors = this.game.input.keyboard.createCursorKeys();
		},

		update: function() {
			
			var controls = this.game.cursors;

			if (controls.left.isDown) {
				this.player.body.rotateLeft(100);
			} else if (controls.right.isDown) {
				this.player.body.rotateRight(100);
			} else {
				this.player.body.setZeroRotation();
			}

			if (controls.up.isDown) {
				this.player.body.thrust(400);
			} else if (controls.down.isDown) {
				this.player.body.reverse(400);
			}

			if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
				this.player.fire();
			}

		}

	}

	return Space;

});