/*
*
*	Boot state for base settings initialization
*
*/

define(['Phaser'], function(Phaser) {

	function Boot(game) {
		this.game = game;
	}

	Boot.prototype = {
		constructor: Boot,

		preload: function() {
			this.stage.disableVisibilityChange = true;
		},

		update: function() {
			this.game.state.start('Preload');
		}

	}

	return Boot;

});