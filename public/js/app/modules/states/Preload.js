/*
*
*	Preload state loads all the assets of the game.
*
*/

define(['Phaser'], function(Phaser) {

	function Preload(game) {
		this.game = game;
		this.ready = false;
	}

	Preload.prototype = {
		constructor: Preload,

		preload: function() {
			
			this.game.load.spritesheet('explosion', 'img/explosion.png', 140, 140);
			this.game.load.image('bg', 'img/space.jpg');
			this.game.load.image('bullet', 'img/bullet.png');

			this.game.load.spritesheet('asteroid3', 'img/asteroid.png', 300, 250);
			this.game.load.spritesheet('asteroid2', 'img/asteroid2.png', 136, 144);
			this.game.load.spritesheet('asteroid1', 'img/asteroid3.png', 75, 69);


			this.game.load.image('ship', 'img/player.png');
			this.game.load.physics('physicsData', 'assets/sprite.json');

			this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
		},

		update: function() {
			if (this.ready) {
				this.game.state.start('Space');
			}
		},

		onLoadComplete: function() {
			this.ready = true;
		}

	}

	return Preload;

});